// ==UserScript==
// @name         CrispSurplus TodayHumor Enhancement
// @namespace    http://crisp-surplus.tistory.com
// @version      0.2
// @description  오유의 다양한 기능들을 개선합니다.
// @downloadURL  https://bitbucket.org/crispsurplus/todayhumor-enhancement/raw/master/CrispSurplusTodayHumorEnhancement.user.js
// @updateURL    https://bitbucket.org/crispsurplus/todayhumor-enhancement/raw/master/CrispSurplusTodayHumorEnhancement.user.js
// @author       CrispSurplus (바삭한잉여)
// @match        http://*.todayhumor.co.kr/*
// @grant        none
// ==/UserScript==

jQuery(window).ready(function () {
    init();
});

jQuery(document).ready(function () {
    init();
});

jQuery(document).ajaxComplete(function () {
    csthe_script_rere();
});

function init() {
    csthe_script_rere();
    //csthe_style_init();
}

function csthe_script_rere() {
    jQuery(".rereMemoWrapperDiv .memoDiv").each(function(i,e){
        if(jQuery(e).find('.csou_ReRe').length == 0) {
            jQuery(e).append('<div class="rereIconDiv csou_ReRe"><img src="http://todayhumor.co.kr/board/images/memo_rere_write.gif" onclick="rere('+e.id.substr(7)+')"></div>');
        }
    });
}

function csthe_style_init() {
    /*jQuery('img[src^="images/accuse_board.gif"],div.okNokBookDiv').attr('style','');
    csthe_style_add('.reply_img{max-width:100%;}');*/
    csthe_style_select();
}

function csthe_style_add(sstr) {
    var bd = jQuery('body'),
        css = bd.find('style#csthe_style');
    if (css.length != 1) {
        jQuery('style#csthe_style').remove();
        bd.append('<style>').attr('id','csthe_style');
        var css = bd.find('style#csthe_style');
    }
    
    css.html(css.html() + sstr);
    return true;
}

function csthe_style_select() {
    var optionTables = $('.viewContent').prevAll('table');
    if (optionTables.length > 0) {
	    optionTables.addClass('csthe_optionTables');
	    optionTables.find('td:contains("옵션")').parents('table').addClass('csthe_option');
	    optionTables.find('td:contains("출처")').parents('table').addClass('csthe_source');
    }
}